## Project status
in development.

# Lisp in Quantum


A curation of quantum programs I’ve created in LISP and collected to further test in Lisp. 

I’ve studied quantum computers through self-learning for several years now. There’s very little universally formalized courses on the matter of quantum computing and quantum programming. So we had to figure it out ourselves. The formation of the quantum computing club I started with my friends back in 2019 certainly helped keep us all going. It started feeling like I was repeatedly teaching introductions of what the difference of classical and quantum was, and the different architectures that are out there to people instead of really deep diving into the knowledge I already knew. 

But it was not until I was helping clear out an old building on 14th st here in Manhattan that came across the book "Meta-Math: The Quest for Omega" by Gregory Chaitlin (a book entirely seeming unrelated to quantum but really engaging! I couldn’t stop reading it on the subway.) that I became inspired to break down the large libraries and frameworks that exist today in big systems like qisket and D-wave’s annealer, and begin quantum programming the math behind it all that I know exists. 

I’m thankful to Stephen for giving away his book to me. 

Here are the products of that journey. 

## Why LISP?

I actually hated LISP and never thought I’d ever use it again. I mainly code in python actually. It’s funny how life works. I think it’s absolutely life-changing now. 

Ironically enough, LISP was the first programming language I learned when I was 18 during my freshman year computer science class taught by Alexander Russell. 

It was actually a more academic flavor of LISP called Scheme, and I really didn’t get why we were learning this language I’d never heard of before instead of getting right into it and making cool stuff with Python or Java. I really hated it. Back then, I don’t think there was even syntactical autocompletion in the IDE we were using, so I kept messing up all the damn parenthesis's involved. There. were. so. many. parentheses. I needed to manually count which parenthesis I was on because there wasn’t even good highlighting really. But then next year I programmed in Java (lol). 

A little less than a decade later, & deeply having learned and understood theory of computation through the years, I really appreciate Professor Russell for teaching us this beautifully simple language. He is a great teacher. I only barely passed his class back then, to be honest. But luckily, I saved his lecture slides. I appreciate just how great it was now, as I pave the road to quantum computation with others in the field. It’s a functional language, not an object-oriented one, and it is so critical to algorithm and design, for anything really. 

Plus, there’s autocompletion IDEs & better syntax highlighting now. 

Thank god.  

## Start here:

How does Lisp work? 

### A side-rant: Then why do so many quantum software integrate Python? That’s what real programmers use.

I went down this path. I tried learning all the Python packages too. I tried running the Jupyter notebooks. There’s a glaring issue which we’re all ignoring though.

Hot take: Python is the HTML of quantum programming currently. People use it because it’s what everyone can learn how to use and has heard of before. But you’re not actually doing anything on the quantum computer with Python. It’s mainly used for classical computing which we’re all able to use to see visible differences. We need to see to believe and so we use Python. Similar to how we use HTML to make websites. But you can’t really know quantum computers by using Python. You’d only be able to make quantum websites. 

I learned HTML when I was 13 on [neopets.com](http://neopets.com) designing my Kacheek’s pet pages. To be honest, it was pretty fun. So I’m not knocking it at all. I was not a computer science major at that point, it was something that any and all of us could learn how to do when we got interested enough as a kid or a teen on [MySpace.com](http://MySpace.com).

But my desktop classical computer was already built upon the shoulders of giants at this point. This was the 2000s and the layman’s Dell that I was using had already evolved from the 1980s box hunk of circuitry that people were trying to commercialize back then. Coding in HTML when the desktop computer of Dell or Apple wasn’t yet commercialized would be illogical. They were dealing with IBM computers at best, sheesh.

We’re dealing with IBM computers here too. 

We’re in the 1980s now of quantum computers right now. I don’t even know how the functions work. I don’t know how the code speaks with the hardware. We don’t even know what hardware we’re using. Superconducting chips? Photons? But what I do know is that developments are happening fast!! Now is the time to start programming. 

That’s why we have to start with the basics. Start with the most stable error-corrected qubits. Create simple lisp operators. Grow them. Create an interpreter. Make it FAST. Make it easy to understand instead of hiding behind some fancy rich company. Companies are scary! There’s too many of them, there’s too much at stake, and they probably all use the actual important code internally. Make it one that makes sense with today’s hardware updates. And then we can use HTML, I mean, Python, to our heart’s content. 

I’m ranting because too many of us get caught in the Python trap. I was there too. Don’t wait for the HTML! This is our time to innovate with the ground-breaking programs of the early C & COBOL programmers. 

That’s right, we’re in the FORTRAN era baby!! 

And LISP was also used back then, just as it can be now. Multiparadigm like I mentioned. 

## Though, a word of caution:

A lot of this will be abstract. Luckily, math is just a descriptor of nature and life. We can take all the ways we view quantum now and make it as simple as possible. And there’s no one way to do it. It’s just what makes sense to you. 

I think that’s what quantum computing is all about anyway. And it’s why it’s so powerful. 

You decide what you’d like to make sense of from the quantum computer. And it’ll tell you from it’s descriptor of nature and life, based on math and quantum mechanics. 

# Let’s get creating!




## Googled it & it’s not just me! (References)

So okay, I think quantum computing companies already know this. They’re building it on top of LISP right? Like, it just makes sense. 

- This [hacker news article](https://news.ycombinator.com/item?id=26873119) on quantum computing and lisp
- This [reddit](https://www.reddit.com/r/lisp/comments/8jy866/lisp_the_quantum_programmers_choice_computerphile/) thread on quantum programmer for lisp!
    - [Lisp, The Quantum Programmer's Choice - Computerphile](https://www.youtube.com/watch?time_continue=34&v=svmPz5oxMlI&feature=emb_logo)
- A [github](https://github.com/fwcd/quantumlisp) for quantum simulator in lisp!
- Rigetti’s [powerpoint](http://www.sbcl.org/sbcl20/slides/robert-smith-rigetti.pdf) on the usefulness of LISP
- More [examples](https://lisp-journey.gitlab.io/who/) that express the power of LISP
- Writing [quantum program](https://forum.devtalk.com/t/write-your-first-quantum-program-common-lisp/9086) in LISP
- heavily inspired by the [quantum LISP papers](https://scholar.google.be/citations?user=R1iIt0AAAAAJ&hl=en) of Ellie D’Hondt, who was really ahead of her time.


## Roadmap
To be added eventually...

## Contributing
To be added eventually...


## Authors and acknowledgment
To all the quantum coding LISP-ers out there.

## License
MIT 

